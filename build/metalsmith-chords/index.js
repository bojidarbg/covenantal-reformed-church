const generator = require('./generator')

let header = `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Песни ЗРЦ</title>
    <link rel="stylesheet" href="/css/songs.css"/>
</head>
<body>
<header id="start">
<h1><a href="#start">Песни</a></h1>

<p>Това са различни песни които пеем/сме пяли в ЗРЦ.</p>

<p><label>
    <span>Търсене:</span>
    <input type="search" id="search-box">
</label></p>
</header>
`

let footer = `<script src="/js/songs.js"></script>
</body>
</html>
`


function metalsmithPlugin() {
    return function(files, metalsmith, done){
        setImmediate(done);

        let contents = [Buffer.from(header)]

        for (var file of Object.keys(files).sort()) if (file.match(/\.chords$/)) {
            let generated = generator(file, files[file].contents.toString('utf8'))
            contents.push(Buffer.from(generated))
            delete files[file];
        }

        contents.push(Buffer.from(footer))

        if (contents.length > 2) {
            files['songs/index.html'] = {
                title: 'Песни',
                draft: false,
                contents: Buffer.concat(contents)
            }
        }
    };
}

module.exports = metalsmithPlugin
