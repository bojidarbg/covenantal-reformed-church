const chordsParser = require('./parser')

function convertFile(filename, contents) {
    let linkTranslations = {
        'source': 'Източник',
        'recording': 'Запис',
        'original': 'Оригинал',
        'notes': 'Ноти',
        'from': 'от'
    }

    let sectionTranslations = {
        'intro': 'Въведение',
        'chorus': 'Припев',
        'verse': 'Куплет',
        'bridge': 'Мост',
        'solo': 'Соло',
        'outro': 'Завършек',
        'full': 'Всичко',
        'repeat': 'Повторение',
        'notes': 'Бележки',
        'links': 'Връзки'
    }

    let escapeHtml = text => text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;')
    let translateSection = name => escapeHtml(name.split(' ').map(x => sectionTranslations[x] !== undefined ? sectionTranslations[x] : x).filter(x => x).join(' '))
    let makeSection = (type, name, content) => '<section class="' + type + '"><div class="section-name">' + translateSection(name) + ':</div>' + content + '</section>'
    let makeRepeat = join => (count, content) => '<span class="repeat"><span class="repeat-count">×' + escapeHtml(count) + '</span>' + content.join(join) + '</span>'
    let contentId = escapeHtml(filename.match(/([^/]+)\.chords$/)[1] || filename)

    return chordsParser(filename, contents, {
        main: parts => '<article id="' + contentId + '"><header>' +
            parts.title.join('\n') +
            '</header><main>' +
            parts.content.join('\n') +
            (parts.text || []).join('\n') +
            '</main><footer>' +
            (parts.notes || []).join('\n') +
            parts.links.join('\n') +
            '</footer></article>\n',
        title: t => '<h2><a class="heading-anchor" href="#' + contentId + '">' + escapeHtml(t).replace(/ \*/, ' <span class="star">*</span>') + '</a></h2>',
        verse: {
            text: t => escapeHtml(t),
            chord: t => [escapeHtml(t)],
            line: parts => {
                let line = ''
                let hasText = false
                let hasChords = false
                let lastWasChord = false
                for (let part of parts) {
                    if (typeof part == 'string') {
                        let isEmpty = (part.trim() == '')
                        hasText = hasText || (!isEmpty && !part.startsWith('<span'))
                        line += part
                    } else if (Array.isArray(part)) { // chord
                        hasChords = true
                        line += '<span class="verse-chord">' + part[0] + '</span>'
                    }
                }
                if (lastWasChord) {
                    line += '</span>'
                }
                if (hasText && !hasChords) {
                    return '<span class="text-only">' + line + '</span>'
                }
                if (!hasText && hasChords) {
                    return '<span class="chords-only">' + line + '</span>'
                }
                return '<span class="line">' + line + '</span>'
            },
            full: (name, lines) => makeSection('verse', name, '<p>' + lines.join('<br>\n') + '</p>'),
            repeat: makeRepeat('<br>\n'),
            section: name => '<span class="verse-repeat">(Свирим ' + translateSection(name[0].replace(/<span class="[^"]+">/g, '').replace(/<\/span>/g, '')).toLowerCase() + ')</span>',
        },
        repeat: {
            section: name => '<span class="verse-repeat">' + translateSection(name) + '</span>',
            full: sections => makeSection('repetition', 'repeat', '<p>' + sections.join(', ').replace(/<\/span>,/g, ',</span>') + '</p>'),
            repeat: makeRepeat(', ')
        },
        links: {
            text: t => escapeHtml(t),
            url: url => escapeHtml(url),
            link: (name, text, url) => text ?
                url ? linkTranslations[name] + ' ' + linkTranslations.from + ' <a href="' + url + '">' + text.trim() + '</a>' : linkTranslations[name] + ' ' + linkTranslations.from + ' ' + text.trim():
                '<a href="' + url + '">' + linkTranslations[name] + '</a>',
            full: links => '<p class="links">\n' + links.join(',\n') + '</p>'
        },
        notes: notes => '<ol class="notes">\n' + notes.map(x=>'<li>' + escapeHtml(x) + '</li>').join('\n') + '</ol>',
        freeform: t => escapeHtml(t) + '<br>'
    });
}

module.exports = convertFile
